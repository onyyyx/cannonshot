﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    public Rigidbody2D gravAffected;

    private float gravity;
    private float seconds;
    private float currentTime;
    private float expectedTime;

	// Use this for initialization
	void Start ()
    {
        gravAffected = GetComponent<Rigidbody2D>();
        
    }

    //Used to calculated physics values before each frame.
    private void FixedUpdate()
    {
        //Not sure if I am going to simplyfy this or 
        //Change this into something else
        //I know its inefficient.
        currentTime = Time.deltaTime;
        expectedTime = Time.deltaTime;
        if (currentTime == expectedTime)
        {
            gravAffected.AddForce(transform.up * gravity);
        }
        
    }

    // Update is called once per frame
    void Update ()
    {
        //turns a force being applied per frame or statically to
        // acceleration.
        gravity = -9.81f * Time.deltaTime;
    }
}
