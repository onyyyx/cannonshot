﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB_View : MonoBehaviour {

    //Serializing the private floats allows them to
    //be edited just like a public.
    [SerializeField]
    private float top;
    [SerializeField]
    private float bottom;
    [SerializeField]
    private float right;
    [SerializeField]
    private float left;
    //It is public so that the cannonball object may be place inside
    //directly through the editor.
    public Transform CB;

	// Use this for initialization.
    //Because we need the transform of the cannonball
    //we redefine CB as the transform of the tethered
    //object cannonball.
	void Start ()
     {
        CB = CB.transform;
	 }
	
	// Update is called once per frame
    // 
	void LateUpdate ()
    {
        transform.position = new Vector3 (Mathf.Clamp(CB.position.x, left, right), Mathf.Clamp(0, bottom, top), transform.position.z);
	}
}
