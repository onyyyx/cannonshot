﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball_view : MonoBehaviour
{
    //
    public GameObject Cannonball;
    private Vector3 DistancefromCB;
    private Transform CamPos;

	// Use this for initialization
	void Start ()
    {
        //Determines distance from the cannonball.
        //----------------------------------------------------------
        DistancefromCB = transform.position - Cannonball.transform.position;
	}

    //
    //private void Update ()
    //{
        //DistancefromCB.position = new Vector3(Cannonball.position, 0, 0);
    //}

    // LateUpdate is called once per frame but runs after
    //all items run in update.
    void LateUpdate ()
    {
        //CamPos.position = new Vector3 (Cannonball.position.x, 0 , 0);

        //Adds the missing distance from the camera to the cannonball
        //At the end of each frame.
        //------------------------------------------------------------
        transform.position = Cannonball.transform.position + DistancefromCB;
	}
}