﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball_Tracking : MonoBehaviour
{

    public Transform cannonball;

    //keep track of state: FirstCannon1OtherCannons2 initializes it, objectState can change
    public int FirstCannon1OtherCannons2; //2 for tracking ball, 1 for tracking finger
    private int objectState;

    //TrackCannonball() variables
    private float lastAngle;
    private float actualAngle = 0F; //0 is up

    //TrackFinger() variables
    public Camera cam;
    public float cannonZRotation=0;

    // Use this for initialization
    void Start()
    {
        objectState = FirstCannon1OtherCannons2;


    }
    
    //Late update so it tracks ball after physics applied to ball
    void LateUpdate()
    {
        if(objectState == 1)
        {
            //Tracks mouse position
            TrackFinger();
        }
        else if(objectState == 2)
        {
            //TrackCannonball() aims cannon at top 120 degrees to follow cannonball
            TrackCannonball();
        }
        else
        {
            //does nothing once fired
        }
        
    }
    

    //dealing with cannonball
    private void OnTriggerEnter2D(Collider2D ball)
    {
        objectState = 1;
        ball.transform.position = transform.position;
        //Debug.Log("trigger entered");
    }

    private void OnTriggerExit2D()
    {
        objectState = 0;
    }


    //Tracks Finger for future aiming
    private void TrackFinger()
    {
        //to simplify code
        float mouseY = Input.mousePosition.y;
        float mouseX = Input.mousePosition.x;

        Vector3 screenPos = cam.WorldToScreenPoint(transform.position);
        float cannonY = screenPos.y;
        float cannonX = screenPos.x;

        //if mouse is between top of screen and cannon && is on screen
        if ( (mouseY > cannonY & mouseY < Screen.height) & 
            (mouseX >= 0 & mouseX <= Screen.width))
        {
            float angle = (Mathf.Atan2((mouseY - cannonY), (mouseX - cannonX)) * Mathf.Rad2Deg) - 90F;

            //if ball has moved, update position as long as it's between 60 (left) and -60 (right)
            if (lastAngle != angle)
            {
                float angleToBall = -(lastAngle - angle); //angleToBall is angle needed to move to aim towards ball
                float testActualAngle = actualAngle + angleToBall;

                if ( (-60F <= testActualAngle) & (0 >= testActualAngle))
                {
                    transform.Rotate(0, 0, angleToBall);
                    lastAngle = angle;
                    actualAngle = testActualAngle;

                    //updates angle of cannon publicly
                    cannonZRotation = angle + 90;
                    //Debug.Log(cannonZRotation);
                }

            }
        }
    }

    //Aims 
    //To optimize use 360degrees% instead of from 0 & rename vars
    private void TrackCannonball()
    {
        //angle is the angle from the cannon to the cannonball
        float angle = (Mathf.Atan2((cannonball.position.y - transform.position.y), (cannonball.position.x - transform.position.x)) * Mathf.Rad2Deg) - 90F;

        //if ball has moved, update position as long as it's between 60 (left) and -60 (right)
        if (lastAngle != angle)
        {
            float angleToBall = -(lastAngle - angle); //angleToBall is angle needed to move to aim towards ball
            float testActualAngle = actualAngle + angleToBall;

            if (-60F <= testActualAngle)
            {
                if (60 >= testActualAngle)
                {
                    transform.Rotate(0, 0, angleToBall);
                    lastAngle = angle;
                    actualAngle = testActualAngle;
                }
            }
            
        }
    }
}



/*
private void Stuff123()
{
    float publicXForce45 = 9F;

    float angle45Force = Mathf.Sqrt( (Mathf.Pow(publicXForce45, 2)) + (Mathf.Pow(publicXForce45, 2)) );

    float forceX = angle45Force * ( Mathf.Cos(cannonZRotation) );
    float forceY = angle45Force * ( Mathf.Sin(cannonZRotation) );
}
*/

