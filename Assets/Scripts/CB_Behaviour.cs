﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB_Behaviour : MonoBehaviour
{

    //Variables declared.
    public float despawnTimer;
    public float xAcceleration;
    public float yAcceleration;
    public float xDrag;
    public float yDrag;
    public float xForce;
    public float yForce;
    public Rigidbody2D CB;
    public Collision2D impact;

    //What initially happens.
    //CB (cannonball) recieves a 2D
    //Ridgedbody.
    void Start()
    {
        CB = GetComponent<Rigidbody2D>();
    }

    //Physics calculations per frame.
    private void FixedUpdate()
    {
        //The transforms are right & up
        //because they are to be positive.
        CB.AddForce(transform.right * yForce);
        CB.AddForce(transform.up * yForce);
    }

    // Update is called once per frame
    void Update()
    {
        //Determines if the left mouse button is
        //pressed. If so then as long as it is held
        //down the x and y forces (xForce & yForce)
        //incrementally move by an amount associated
        //with acceleration (xAcceleration & yAcceleration).
        if (Input.GetMouseButton(0))
        {
            xForce += xAcceleration;
            yForce += yAcceleration;
        }

        //At the point of left mouse button release
        //the force of the cannonball is decremented
        //by the variable drag (xDrag & yDrag) so
        //long as the force variables are greater than
        //0.
        if (Input.GetMouseButtonUp(0))
        {
            while (xForce > 0 || yForce > 0)
            {
                if (xForce > 0)
                {
                    xForce -= xDrag;
                }
                if (yForce > 0)
                {
                    yForce -= yDrag;
                }

            }
        }

        /* David's in progress code.
        function OnCollisionEnter(collision : Collision)
        {
            if(collision.gameObject.name == "Cannon")
            {
                Destroy(gameObject);
            }
            yeild Waitforseconds(despawnTimer);
            Destroy(CB);
        }
        */
    }
}