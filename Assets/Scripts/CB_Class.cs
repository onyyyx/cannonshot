﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CB_Class : MonoBehaviour {

    //Variables are declared and called.
    //Public:
    public Rigidbody2D CB;
    public float publicXForce45 = 10f;

    //Private:
    private float cannonAngle;
    private float angxForce;
    private float angyForce;
    private bool triggerOn;
    private GameObject gravityScrpt;
    private float forceX;
    private float forceY;

    //Void Start () is What happens when the program first starts.
    //By tracking the object "Cannon (0)" we access the script "Cannonball_Tracking"
    //Then we get the specific part in that script of "cannonZRotation"
    //And we apply it to "cannonAngle".
    //CB is created and a Rigidbody2D is applied to it.
    //The "Gravity" script is disabled right away.
    void Start ()
    {
        CB = GetComponent<Rigidbody2D>();
        gameObject.GetComponent<Gravity>().enabled = false;
	}

    //Void Update occurs every frame.
    //Once every frame the program gets the position of the cannonball
    //"angxForce" is calculated by taking the angle of the cannon in question
    //And multiplying it by 0.1.
    //The same general thing is applied to angyForce however we also add 4..
    //It applies this angle to the equations to get our y and x forces.
    private void Update()
    {
        cannonAngle = GameObject.Find("Cannon (0)").GetComponent<Cannonball_Tracking>().cannonZRotation;

        float angle45Force = Mathf.Sqrt((Mathf.Pow(publicXForce45, 2)) + (Mathf.Pow(publicXForce45, 2)));

        forceX = angle45Force * (Mathf.Cos(cannonAngle));
        forceY = angle45Force * (Mathf.Sin(cannonAngle));
        
       // angxForce = 0.1f * cannonAngle;
       // angyForce = 0.01f * cannonAngle + 4;
    }
    
    //When this object collides with another then triggerOn becomes true.
    private void OnTriggerEnter2D(Collider2D other)
    {
        triggerOn = true;
    }

    //When this object stops colliding with another triggerOn becomes false.
    private void OnTriggerExit2D(Collider2D other)
    {
        triggerOn = false;
    }

    //FixedUpdate is used in physics calculations before each frame.
    //If the mouse button is pressed, then, a force previously calculated is
    //Applied to both the right and up seperately but at the same time.
    //The mouse button also activates the previously deactivated script "Gravity".
    //If the cannonball hits an istrigger object then "Gravity" is deactivated,
    //Otherwise it remains on.
    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            gameObject.GetComponent<Gravity>().enabled = true;
            CB.AddForce(transform.right * forceX);
            CB.AddForce(transform.up * forceY);

            if (triggerOn == false)
            {
                gameObject.GetComponent<Gravity>().enabled = true;
            }
            if (triggerOn == true)
            {
                gameObject.GetComponent<Gravity>().enabled = false;
            }
        } 
    }
}
