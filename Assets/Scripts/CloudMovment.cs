﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovment : MonoBehaviour {

    public float Windspeed;
    public Rigidbody2D Cl1;
	
    // Use this for initialization
	void Start ()
    {
        Cl1 = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	private void FixedUpdate ()
    {
        Cl1.AddForce(transform.right * Windspeed);
	}
}
